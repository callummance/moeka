from flask import Flask, session, Response
from flask import render_template, request

import settings
import json
from construct_mail import email_content

app = Flask(__name__)
config = settings.load_config("settings.conf")

user_sessions = {}


def get_session_obj(session):
    if 'content' not in session:
        content = email_content.Content()
        new_session = email_content.UserSession(content, user_sessions)
        session['content'] = new_session.sess_key
        return new_session
    elif session['content'] not in user_sessions:
        content = email_content.Content()
        new_session = email_content.UserSession(content, user_sessions)
        session['content'] = new_session.sess_key
        return new_session
    else:
        return user_sessions[session['content']]


@app.route('/')
def home():
    output = render_template("homepage.html")
    return output


@app.route('/edit')
def edit_mail():
    session_obj = get_session_obj(session)
    content_type = request.args.get('template')
    if content_type == 'weekly':
        session_obj.content.forecast_init()
    elif content_type == 'blank':
        session_obj.content.blank_init()

    output = render_template("Editor/webroot_template.html")
    return output


@app.route('/get_content', methods=["GET"])
def get_content():
    session_obj = get_session_obj(session)
    resp = Response(response=session_obj.content.tostring_json(),
                    status=200,
                    mimetype="application/json")
    return resp


@app.route('/get_title', methods=["GET"])
def get_title():
    session_obj = get_session_obj(session)
    resp = Response(response=json.dumps(session_obj.content.title),
                    status=200,
                    mimetype="application/json")
    return resp


@app.route('/get_subtitle', methods=["GET"])
def get_subtitle():
    session_obj = get_session_obj(session)
    resp = Response(response=json.dumps(session_obj.content.date),
                    status=200,
                    mimetype="application/json")
    return resp


@app.route('/mail')
def view_mail():
    s = get_session_obj(session)
    content = s.content

    output = render_template("Mail/root_template.html",
                             colourConf=config["colours"],
                             contentConfig=config["content"],
                             content=content)
    return output


app.secret_key = b'\xc4=/\xcb\xa2\xae\xaeJ\xba6\xc6\xb5\x8d\xbf<\x1f\xe9\xf2\xa9\x01\x93\x88\xc5I'

if __name__ == '__main__':
    app.run(debug=True)
