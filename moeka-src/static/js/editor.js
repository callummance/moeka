/**
 * Created by Callum on 23/05/2016.
 */

$(document).ready(function() {
    //Get Title
    $.getJSON("/get_title", function (data) {
        $("#email_title").attr("value", data);
    });

    //Get Subtitle
    $.getJSON("/get_subtitle", function (data) {
        $("#email_subtitle").attr("value", data);
    });

    //Get section content
    $.getJSON("/get_content", function (data) {
        insert_sections($('#content_acc'), data);
    });


    //Markdown Editor
    var mde = new SimpleMDE({ element: document.getElementById("mdeditor") });

    //Accordion
    $('#content').on('click', '.toggle', function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp(350);
        } else {
            $this.parent().parent().find('li .inner').removeClass('show');
            $this.parent().parent().find('li .inner').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
        }
    });
});

//Insert a list of sections into an element
function insert_sections(elem, sections){
    $.each(sections, function (i, section) {
        var sectionTitle = section["title_txt"];
        console.log(section);
        elem.append(
            "<li>" +
                "<a class='toggle' href='javascript:void(0);'>" + sectionTitle + "</a>" +
                "<ul class='inner'>" +
                    "content" +
                "</ul>" +
            "</li>"
        );
    });
}