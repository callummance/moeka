from flask import Markup

from construct_mail.section import Section

'''Selection of predefined sections likely to see regular use'''

highlights_section = Section(
                        "<img src=\"https://www.union.ic.ac.uk/scc/animesoc/Images/email/Highlights.png\" alt=\"Highlights & Reminders\" style=\"font-size: 24px; line-height: 28px; font-weight: bold; width:100%; max-width:500px\">",
                        "",
                        "Mail/section_with_image_title_template.html",
                        "Highlights and Reminders",
                        "",
                        "Mail/section_with_title.txt",
                        )

events_section = Section(
                        "<img src=\"https://www.union.ic.ac.uk/scc/animesoc/Images/email/Events.png\" alt=\"Events\" style=\"font-size: 24px; line-height: 28px; font-weight: bold; width:100%; max-width:500px\">",
                        "",
                        "Mail/section_with_image_title_template.html",
                        "Events",
                        "",
                        "Mail/section_with_title.txt",
                        )


def insert_titled_section(supersection, title, content):
    title_plain = Markup(title).striptags()
    content_plain = Markup(content).striptags()
    new_section = Section(title, content, "Mail/section_with_title_template.html",
                          title_plain, content_plain, "Mail/section_with_title_template.txt")
    new_section.section_level = supersection.section_level + 1
    new_section.subsections = []
    supersection.add_subsection(new_section)
    return


def insert_untitled_section(supersection, content):
    content_plain = Markup(content).striptags()
    new_section = Section("", content, "Mail/section_without_title_template.html",
                          "", content_plain, "Mail/section_without_title_template.txt")
    new_section.section_level = supersection.section_level + 1
    supersection.add_subsection(new_section)
    return
