from flask import render_template
import json

font_sizes = [33, 24, 18, 16]


class Section:
    image_title = False
    section_level = 0

    subsections = []

    title_htm = "Section header"
    title_txt = "Section header"

    template_htm = ""
    template_txt = ""

    content_htm = ""
    content_txt = ""
    content_htm_orig = ""
    content_txt_orig = ""

    def __init__(self,  title_html, content_html, template_html, title_txt, content_txt, template_txt):
        self.title_htm = title_html
        self.title_txt = title_txt
        self.content_htm_orig = content_html
        self.content_txt_orig = content_txt
        self.template_htm = template_html
        self.template_txt = template_txt
        self.subsections = []
        return

    def tostring_htm(self):
        self.render_subsections()
        font_size = font_sizes[3 if self.section_level > 3 else self.section_level]
        return render_template(self.template_htm, content=self, font_size=font_size)

    def tostring_txt(self):
        self.render_subsections()
        return render_template(self.template_txt, content=self)

    def tostring_json(self):
        return json.dumps(self.__dict__)

    def tostring_md(self):
        self.render_subsections_md()

    def add_subsection(self, section):
        self.subsections.append(section)
        return


    def render_subsections(self):
        self.content_htm = self.content_htm_orig
        self.content_txt = self.content_txt_orig
        for section in self.subsections:
            self.content_htm += section.tostring_htm()
            self.content_txt += section.tostring_txt()
        return
