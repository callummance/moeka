import base64
import os
import datetime
import json

from construct_mail import default_sections

key_bits = 128


class UserSession:

    def __init__(self, content, sessions):
        sess_key = os.urandom(key_bits)
        self.sess_key = base64.b64encode(sess_key)

        self.content = content
        sessions[self.sess_key] = self
        return


class Content:
    title = "Email Title"
    date = "Date"
    sections = []

    config = None
    sender = None

    def __init__(self):
        return

    def tostring_htm(self):
        res = ""
        for section in self.sections:
            res += section.tostring_htm()
        return res

    def tostring_txt(self):
        res = ""
        for section in self.sections:
            res += section.tostring_txt()
        return res

    def tostring_json(self):
        sections_serial = list(map(lambda s: s.tostring_json(), self.sections))
        return "[" + ",".join(sections_serial) + "]"

    def add_section(self, section):
        self.sections.append(section)
        return

    def reset_content(self):
        self.sections = []
        return

    def forecast_init(self):
        self.sections = []
        self.title = "AniSoc Weekly Forecast"
        self.date = get_forecast_dates()
        self.add_section(default_sections.highlights_section)
        self.add_section(default_sections.events_section)
        return

    def blank_init(self):
        self.sections = []
        self.title = "Email Title"
        self.date = "Email Date"


def get_next_weekday(day):
    dt = datetime.datetime.now().replace(hour=0, minute=0, second=0)
    days_remaining = (day - dt.weekday() - 1) % 7 + 1
    return dt + datetime.timedelta(days_remaining)

def get_forecast_dates():
    start = get_next_weekday(0)
    end = start + datetime.timedelta(7)

    start_str = start.strftime("%d %B")
    end_str = end.strftime("%d %B")

    return start_str + " to " + end_str



