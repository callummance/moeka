import smtplib


class Mailer:
    smtpObj = None
    sender = ""

    def __init__(self, config):
        mailConfig = config['mailer']
        #self.smtpObj = smtplib.SMTP(mailConfig['MailHost'], mailConfig['MailPort'])
        self.sender = mailConfig['MailSender']
        self.target = mailConfig['MailTarget']

    def send_mail(self, content):
        message_html = content.tostring_htm()
        message_txt = content.tostring_txt()
        message = message_html

        self.smtpObj.sendmail(self.sender, self.target, message)
