import configparser

configFile = "settings.conf"


def load_config(file=configFile):
    config = configparser.ConfigParser()
    config.read(file)
    return config
